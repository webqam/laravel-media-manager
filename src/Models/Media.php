<?php

namespace TalvBansal\MediaManager\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

/**
 * @property \Carbon\Carbon $created_at
 * @property int $id
 * @property \Carbon\Carbon $updated_at
 */
class Media extends Model
{

    protected $guarded = ['id'];



    /**
     * @param String $path
     * @return mixed Model|null
     */
    public static function get(String $path)
    {
        $path = config('laravel-media-manager.basePath') . '/' . $path;
        return self::where('path', self::cleanPath($path))->first();
    }

    /**
     * @param String $path
     * @return bool
     */
    public static function exists(String $path)
    {
        $path = config('laravel-media-manager.basePath') . '/' . $path;
        return self::where('path', self::cleanPath($path))->count() > 0;
    }

    /**
     * @param String $path
     * @return string
     */
    protected static function cleanPath(String $path)
    {
        return '/' . ltrim($path, '/');
    }

    /**
     * @param String $path
     */
    public static function removeMedia(String $path)
    {
        if (($media = self::getMediaFromPath($path)) !== null) {
            $media->delete();
        }
    }

    /**
     * @param String $path
     * @return mixed
     */
    public static function getMediaFromPath(String $path)
    {
        $path = config('laravel-media-manager.basePath') . $path;
        return self::where('path', self::cleanPath($path))->first();
    }

    public static function updatePathFolder(String $oldPath, String $newPath)
    {
        self::renameFile($oldPath, $newPath);
    }

    /**
     * @param String $oldPath
     * @param String $newPath
     */
    public static function renameFile(String $oldPath, String $newPath)
    {
        if (($media = self::getMediaFromPath($oldPath)) !== null) {
            $media->path = config('laravel-media-manager.basePath') . $newPath;
            $media->save();
        }
    }

    public static function add(String $path)
    {
        $realPath = Storage::disk('public')->path(ltrim($path, '/'));
        $imageInfo = self::getMetas($realPath);

        $path = config('laravel-media-manager.basePath') . $path;
        $media = new static();
        $media->fill(array_merge($imageInfo, ['path' => self::cleanPath($path), 'enabled' => true]));
        $media->save();
    }

    /**
     * Get image metadata
     *
     * @param string $imagePath
     * @return array
     */
    protected static function getMetas(String $imagePath): array
    {
        $copyright = '';
        try {
            $info = exif_read_data($imagePath);
            $copyright = isset($info['COMPUTED']['Copyright']) ? $info['COMPUTED']['Copyright'] : '';
        } catch (\Exception $e) {
        }

        $info = getimagesize($imagePath);

        return [
            'mime_type' => $info['mime'],
            'width' => $info[0],
            'height' => $info[1],
            'copyrights' => $copyright,
            'size' => filesize($imagePath)
        ];
    }
}
